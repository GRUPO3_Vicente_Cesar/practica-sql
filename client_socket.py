import socket
import re, uuid
from cryptography.fernet import Fernet

import random
import datetime




HEADER = 64
PORT = 3074
SERVER = 'localhost'
ADDR = (SERVER, PORT)
FORMAT = 'utf-8'
DISCONNECT_MESSAGE = 'DISCONNECT!'
MAC = (':'.join(re.findall('..', '%012x' % uuid.getnode()))) 

def load_key():
    """
    Loads the key named `secret.key` from the current directory.
    """
    return open("secret.key", "rb").read()

key = load_key()

client = socket.socket(socket.AF_INET, socket.SOCK_STREAM)

client.connect(ADDR)

def send(msg):
    message=msg.encode(FORMAT)  #codifica el mensaje
    f = Fernet(key)
    encrypted_message = f.encrypt(message) #Encripta
    msg_length = len(encrypted_message)    #tamaño del mensaje encriptado
    send_length = str(msg_length).encode(FORMAT) 

    send_length += b' '*(HEADER-len(send_length)) 
    client.send(send_length)
    client.send(encrypted_message)

    print(client.recv(2048).decode(FORMAT))

send(MAC)


#GENERACION DE DATOS
#datos = [id_sensor, temperatura, humedad, SampleTime]     


datos = []

for x in range(5):    
    id_sensor = x 
    temperatura = round(random.uniform(0.00 , 100.00), 2)
    humedad = random.randint(0,100)
    humedad = f'{humedad}%'
    Sampletime = f'{datetime.timedelta(seconds=1)}'
    data2 = [x,temperatura,humedad,Sampletime]
    datos.append(data2)

datos = f'{datos}'

send(datos)

send(DISCONNECT_MESSAGE)

