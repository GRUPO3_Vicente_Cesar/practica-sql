import socket
import threading
from cryptography.fernet import Fernet
import pyodbc
import datetime

HEADER = 64
PORT = 3074
SERVER = 'localhost'
ADDR = (SERVER, PORT)
FORMAT = 'utf-8'
DISCONNECT_MESSAGE = 'DISCONNECT!'

def load_key():
    """
    Loads the key named `secret.key` from the current directory.
    """
    return open("secret.key", "rb").read()


key = load_key()



server = socket.socket(socket.AF_INET, socket.SOCK_STREAM)

server.bind(ADDR)




#SQL CONFIGURACIONES PREVIAS


import ast 
#INSERTAR EN LA TABLA sensor
data1 = []
data2 = []
k = 0 

def SQL(datos):
    
    ini_list = datos
    # Converting string to list 
    res = ast.literal_eval(ini_list)
   
    server = 'localhost'
    database = 'tutorial'

    cnxn = pyodbc.connect('DRIVER={ODBC Driver 17 for SQL Server}; \
                    SERVER=' + server + '; \
                    DATABASE=' + database + ';\
                    Trusted_Connection=yes;')

    cursor = cnxn.cursor()

    insert_query = ''' INSERT INTO sensor (id_sensor,temperatura,humedad,SampleTime,fecha_ingreso)
                        VALUES (?,?,?,?,?);'''


    for row in res:
        time_now = f'{datetime.datetime.now()}'
        values = (row[0],row[1],row[2],row[3],time_now)
        cursor.execute(insert_query,values)
    cnxn.commit()
    cursor.execute('SELECT * FROM sensor')

    for row in cursor:
        print(row)


def handle_client(conn, addr):
    print(f"[NEW CONNECTION] {addr} Autorizando.")
    connected = True
    i = 0
    while connected:
        msg_length = conn.recv(HEADER).decode(FORMAT) #Tamaño del mensaje encriptado
        if msg_length:
            msg_length = int(msg_length) 
            msg = conn.recv(msg_length) #recibe el mensaje encriptado con su tamaño correspondiente
            f = Fernet(key)
            decrypted_message = f.decrypt(msg) #desencripta el mensaje
            msg = decrypted_message.decode(FORMAT) #decodifica el mensaje

            if i == 0:
                if msg != "b0:5c:da:dc:98:13":
                    conn.send("Conexion fallida".encode(FORMAT))
                    connected = False

                print(f"[NEW CONNECTION] {addr} Conectado.")  
                conn.send("Conexion establecida".encode(FORMAT))

            if msg == DISCONNECT_MESSAGE :
                connected = False

                #IMPRIME LA TABLA CREADA
                print(f"[{addr}] {msg}")
                conn.send("Chao".encode(FORMAT))

            if i != 0 and msg != DISCONNECT_MESSAGE:
                SQL(msg)
                conn.send("Msg received".encode(FORMAT))

            
            i += 1


    conn.close()


def start():
    server.listen()
    print(f"[LISTEN] Server is listening on address {ADDR}")
    while True:
        conn, addr = server.accept()
        thread = threading.Thread(target=handle_client, args=(conn, addr))
        thread.start()
        print(f"[ACTIVE CONNECTIONS] {threading.activeCount() - 1}")

print("[STARTING] server is running.....")
start()
