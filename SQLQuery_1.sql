-- CREAR LA BASE DE DATOS TUTORIAL
-- Create a new database called 'tutorial'
-- Connect to the 'master' database to run this snippet
USE master
GO
-- Create the new database if it does not exist already
IF NOT EXISTS (
    SELECT [name]
        FROM sys.databases
        WHERE [name] = N'tutorial'
)
CREATE DATABASE tutorial
GO





-- CREAR LA TABLA
CREATE TABLE sensor(
    id_sensor INT NOT NULL PRIMARY KEY, -- Primary Key column
    temperatura FLOAT NOT NULL,
    humedad VARCHAR (255) NOT NULL,
    SampleTime VARCHAR (255) NOT NULL,
    fecha_ingreso VARCHAR (255) NOT NULL
)



-- Select rows from a Table or View '[sensor]' in schema '[dbo]'
SELECT * FROM [dbo].[sensor]
GO

